package hzt.model;

import javafx.geometry.Dimension2D;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import lombok.Getter;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static javafx.scene.paint.Color.*;

public final class AppConstants {

    private static final Logger LOGGER = LogManager.getLogger(AppConstants.class);

    private static final Properties PROPS = configProperties();
    public static final int INIT_FRAME_RATE = 30; // f/s
    public static final int INIT_NUMBER_OF_SHAPES = 5000;
    public static final int INIT_SEED_SIZE = 3;
    public static final int INIT_FLOWER_RADIUS = 250;

    public static final double GOLDEN_RATIO = (1 + Math.sqrt(5)) / 2; // 1.618...
    public static final double DISTRIBUTION_FACTOR = .5;
    public static final int INIT_FIBONACCI_NUMBER_INDEX = 6;

    public static final Duration INIT_FRAME_DURATION = Duration.seconds(1. / INIT_FRAME_RATE); // s/f

    public static final Color INIT_BG_COLOR = NAVY;
    public static final Color INIT_SHAPE_COLOR = ORANGE;
    public static final Color INIT_HIGHLIGHTING_COLOR = RED;

    public static final Dimension2D MIN_STAGE_DIMENSION = new Dimension2D(750, 500);
    public static final Dimension2D INIT_SCENE_DIMENSION = new Dimension2D(1200, 800);
    public static final Dimension2D INIT_ANIMATION_PANE_DIMENSION = new Dimension2D(640, 640);

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String TITLE = "Fibonacci sim";
    public static final String DOTTED_LINE = "----------------------------------------------------------------------------------------\n";
    public static final String CLOSING_MESSAGE = ANSI_BLUE + "See you next Time! :)" + ANSI_RESET;

    private AppConstants() {
    }

    public static String getProperty(String property, String defaultVal) {
        return PROPS.getProperty(property, defaultVal);
    }

    public static double parsedDoubleProp(String property, double defaultVal) {
        double value = defaultVal;
        String propertyVal = PROPS.getProperty(property);
        if (propertyVal != null) {
            try {
                value = Double.parseDouble(propertyVal);
            } catch (NumberFormatException e) {
                LOGGER.warn(String.format("Property '%s' with value '%s' could not be parsed to a double... " +
                        "Falling back to default: %f...", property, propertyVal, defaultVal));
            }
        } else {
            LOGGER.warn(String.format("Property '%s' not found. Falling back to default: %f",
                    property, defaultVal));
        }
        return value;
    }

    private static Properties configProperties() {
        Properties properties = new Properties();
        String pathName = "./src/main/resources/app.properties";
        File file = new File(pathName);
        try (InputStream stream = new BufferedInputStream(new FileInputStream(file))) {
            properties.load(stream);
        } catch (IOException e) {
            LOGGER.warn(pathName + " not found...", e);
        }
        return properties;
    }

    @Getter
    public enum Scene {

        MAIN_SCENE("mainScene.fxml"),
        ABOUT_SCENE("aboutScene.fxml");

        private final String fxmlFileName;

        Scene(String fxmlFileName) {
            this.fxmlFileName = fxmlFileName;
        }

        public String formattedName() {
            return this.name().charAt(0) + this.name().substring(1).toLowerCase().replace("_", " ");
        }

    }

}

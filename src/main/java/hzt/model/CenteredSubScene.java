package hzt.model;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.SubScene;

public class CenteredSubScene extends SubScene {

    private final DoubleProperty centerX = new SimpleDoubleProperty();
    private final DoubleProperty centerY = new SimpleDoubleProperty();

    public CenteredSubScene(Parent root, double width, double height) {
        super(root, width, height);
        configureCenterProperty();
        bindToCenter(root);
    }

    private void configureCenterProperty() {
        widthProperty().addListener((o2, n2, newWidth) -> centerX.set(newWidth.doubleValue() / 2));
        heightProperty().addListener((o2, n2, newHeight) -> centerY.set(newHeight.doubleValue() / 2));
    }

    private void bindToCenter(Node node) {
        node.translateXProperty().bind(centerX);
        node.translateYProperty().bind(centerY);
    }
}

package hzt.model.fibonacci;

import hzt.model.AnimationAttribute;
import hzt.model.SmartShape;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import org.jetbrains.annotations.NotNull;

import java.util.Iterator;

public class FibonacciGroup implements Iterable<SmartShape> {

    private final Group members = new Group();

    private final IntegerProperty highlightingValue = new SimpleIntegerProperty();
    private final IntegerProperty memberSize = new SimpleIntegerProperty();

    private final DoubleProperty angleFraction = new SimpleDoubleProperty();
    private final DoubleProperty distributionFactor = new SimpleDoubleProperty();
    private final DoubleProperty shapeSize = new SimpleDoubleProperty();
    private final DoubleProperty spiralGroupDimension = new SimpleDoubleProperty();

    private final ObjectProperty<Color> objectColor = new SimpleObjectProperty<>();
    private final ObjectProperty<Color> highlightingColor = new SimpleObjectProperty<>();
    private final ObjectProperty<AnimationAttribute> animationAttribute = new SimpleObjectProperty<>();

    public FibonacciGroup() {
        super();
        memberSize.addListener((observableValue, number, t1) -> controlMembersSize(t1.intValue()));
        members.getChildren().addListener(this::onMemberSizeChanged);
    }

    private void onMemberSizeChanged(ListChangeListener.Change<? extends Node> change) {
        int index = 0;
        for (Node node : change.getList()) {
            setHighlighting((SmartShape) node, index);
            index++;
        }
    }

    public void controlMembersSize(int numberOfBalls) {
        while (members.getChildren().size() != numberOfBalls) {
            if (members.getChildren().size() < numberOfBalls) {
                addSmartShapeToGroup();
            } else {
                removeShape();
            }
        }
        updatePosition();
    }

    private void addSmartShapeToGroup() {
        SmartShape shape = createSmartShape(shapeSize, objectColor.get());
        members.getChildren().add(shape);
    }

    private static SmartShape createSmartShape(DoubleProperty shapeSize, Color color) {
        Circle circle = new Circle();
        circle.radiusProperty().bind(shapeSize);
        return new SmartShape(circle);
    }

    public void updatePosition() {
        int counter = 0;
        for (SmartShape child : this) {
            setPosition(child, counter, getMembers().getChildren().size());
            counter++;
        }
    }

    public void updateHighlighting() {
        int counter = 0;
        for (SmartShape child : this) {
            setHighlighting(child, counter);
            counter++;
        }
    }

    private void setHighlighting(SmartShape child, int counter) {
        final var highLighted = highlightingValue.get() != 0 && counter % highlightingValue.get() == 0;
        child.getBody().fillProperty().bind(highLighted ? highlightingColor : objectColor);
    }

    public void setPosition(Node shape, double counter, int numberOfBalls) {
        double distance = Math.pow(counter / numberOfBalls, distributionFactor.get()) * spiralGroupDimension.get();
        double angle = 2 * Math.PI * angleFraction.get() * counter;
        double xCircle = distance * Math.cos(angle);
        double yCircle = distance * Math.sin(angle);
        shape.setTranslateX(xCircle);
        shape.setTranslateY(yCircle);
    }

    private void removeShape() {
        ObservableList<Node> list = members.getChildren();
        SmartShape shape = (SmartShape) list.get(0);
        shape.getBody().fillProperty().unbind();
        members.getChildren().remove(shape);
    }

    public void loopUpdate() {
        animationAttribute.get().updateSliderValue();
    }

    public void reset() {
        //when something needs to be reset
    }

    @NotNull
    @Override
    public Iterator<SmartShape> iterator() {
        return members.getChildren().stream().map(SmartShape.class::cast).iterator();
    }

    public IntegerProperty memberSizeProperty() {
        return memberSize;
    }

    public double getAngleFraction() {
        return angleFraction.get();
    }

    public DoubleProperty angleFractionProperty() {
        return angleFraction;
    }

    public double getDistributionFactor() {
        return distributionFactor.get();
    }

    public DoubleProperty distributionFactorProperty() {
        return distributionFactor;
    }

    public DoubleProperty spiralGroupDimensionProperty() {
        return spiralGroupDimension;
    }

    public DoubleProperty shapeSizeProperty() {
        return shapeSize;
    }

    public ObjectProperty<Color> objectColorProperty() {
        return objectColor;
    }

    public ObjectProperty<Color> highlightingColorProperty() {
        return highlightingColor;
    }

    public ObjectProperty<AnimationAttribute> animationAttributeProperty() {
        return animationAttribute;
    }

    public AnimationAttribute getAnimationAttribute() {
        return animationAttribute.get();
    }

    public int getHighlightingValue() {
        return highlightingValue.get();
    }

    public IntegerProperty highlightingValueProperty() {
        return highlightingValue;
    }

    public Group getMembers() {
        return members;
    }

}

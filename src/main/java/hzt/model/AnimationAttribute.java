package hzt.model;

import javafx.scene.control.Slider;

public abstract class AnimationAttribute {

    private final String name;
    private final double minValue;
    private final double maxValue;
    private final double initValue;

    protected AnimationAttribute(String name, double minValue, double maxValue, double initValue) {
        this.name = name;
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.initValue = initValue;
    }

    public abstract void updateSliderValue();

    // first set the min and max value, after that the current value
    public void updateIncrementSliderBounds(Slider slider) {
        slider.setMin(minValue);
        slider.setMax(maxValue);
        slider.setValue(initValue);
    }

    public String toString() {
        return name;
    }

}

package hzt.model;

import javafx.scene.Group;
import javafx.scene.shape.Shape;
import org.jetbrains.annotations.NotNull;

/**
 * this class has a natural ordering that is inconsistent with equals."
 */
public class SmartShape extends Group implements Comparable<SmartShape> {

    private final Shape body;

    public SmartShape(Shape body) {
        this.body = body;
        super.getChildren().add(body);
    }

    public Shape getBody() {
        return body;
    }

    @Override
    public int compareTo(@NotNull SmartShape o) {
        Double thisSquareDistanceFromOrigin = this.getTranslateX() * this.getTranslateX() + this.getTranslateY() * this.getTranslateY();
        Double otherSquareDistanceFromOrigin = o.getTranslateX() * o.getTranslateX() + o.getTranslateY() * o.getTranslateY();
        return thisSquareDistanceFromOrigin.compareTo(otherSquareDistanceFromOrigin);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}

package hzt.service;

import hzt.model.fibonacci.FibonacciGroup;
import javafx.animation.AnimationTimer;
import javafx.scene.control.Label;
import javafx.util.Duration;
import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static hzt.model.AppConstants.INIT_FRAME_RATE;
import static java.lang.String.format;

public class StatisticsService {

    private static final Integer[] FIBONACCI_NUMBERS = {1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89};
    private static final String TWO_DEC_DOUBLE = "%-4.2f";

    private final Set<Integer> fibonacciNumberSet = new HashSet<>(Arrays.asList(FIBONACCI_NUMBERS));
    private final GeneralStatsLabelDto generalStatsDto;
    private final FibonacciGroupLabelDto fibonacciGroupLabelDto;
    private final SimpleFrameRateMeter frameRateMeter = new SimpleFrameRateMeter();

    public StatisticsService(GeneralStatsLabelDto generalStatsDto, FibonacciGroupLabelDto fibonacciGroupLabelDto) {
        this.generalStatsDto = generalStatsDto;
        this.fibonacciGroupLabelDto = fibonacciGroupLabelDto;
        frameRateMeter.initialize();
    }

    public void showGlobalStatistics(Duration runTimeSim, double animationIncrement) {
        generalStatsDto.getFrameRateLabel().setText(format(TWO_DEC_DOUBLE + " f/s", frameRateMeter.frameRate));
        generalStatsDto.getRunTimeLabel().setText(format("%-4.3f seconds", runTimeSim.toSeconds()));
        generalStatsDto.getAnimationIncrementLabel().setText(format("%-4.2e increment/s", animationIncrement));
    }

    public void showGroupStatistics(FibonacciGroup fibonacciGroup) {
        fibonacciGroupLabelDto.getGroupSizeLabel().setText(format("%d", fibonacciGroup.getMembers().getChildren().size()));
        int highlightingValue = fibonacciGroup.getHighlightingValue();
        Label highLightingLabel = fibonacciGroupLabelDto.getHighlightingLabel();
        highLightingLabel.setText(format("%d", highlightingValue));
        highLightingLabel.setStyle(fibonacciNumberSet.contains(highlightingValue) ?
                "-fx-text-fill: red;" : highLightingLabel.getScene().getRoot().getStyle());
        fibonacciGroupLabelDto.getTurnFractionLabel().setText(format("%4.7f", fibonacciGroup.getAngleFraction()));
        fibonacciGroupLabelDto.getDistributionLabel().setText(format("%4.3f", fibonacciGroup.getDistributionFactor()));
    }

    private static class SimpleFrameRateMeter {

        private final long[] frameTimes = new long[100];
        private int frameTimeIndex = 0;
        private boolean arrayFilled = false;
        private double frameRate = INIT_FRAME_RATE;

        private void initialize() {
            AnimationTimer frameRateTimer = new AnimationTimer() {

                @Override
                public void handle(long now) {
                    long oldFrameTime = frameTimes[frameTimeIndex];
                    frameTimes[frameTimeIndex] = now;
                    frameTimeIndex = (frameTimeIndex + 1) % frameTimes.length;
                    if (frameTimeIndex == 0) {
                        arrayFilled = true;
                    }
                    if (arrayFilled) {
                        long elapsedNanos = now - oldFrameTime;
                        long elapsedNanosPerFrame = elapsedNanos / frameTimes.length;
                        frameRate = 1e9 / elapsedNanosPerFrame;
                    }
                }
            };
            frameRateTimer.start();
        }

    }

    @Getter
    @Setter
    public static class FibonacciGroupLabelDto {

        private Label groupSizeLabel;
        private Label turnFractionLabel;
        private Label highlightingLabel;
        private Label distributionLabel;

        public FibonacciGroupLabelDto() {
            super();
        }

    }

    @Getter
    @Setter
    public static class GeneralStatsLabelDto {

        private Label frameRateLabel;
        private Label runTimeLabel;
        private Label animationIncrementLabel;

        public GeneralStatsLabelDto() {
            super();
        }
    }
}

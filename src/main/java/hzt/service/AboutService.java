package hzt.service;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.String.format;

public class AboutService {

    private static final Logger LOGGER = LogManager.getLogger(AboutService.class);
    private static final String RELATIVE_TEXT_RESOURCE_DIR = "../../about";

    public List<AboutText> loadContent() {
        List<AboutText> aboutTexts = new ArrayList<>();
        final var url = getClass().getResource(RELATIVE_TEXT_RESOURCE_DIR);
        if (url != null) {
        File fileDir = new File(url.getFile());
            if (fileDir.isDirectory()) {
                for (String fileName : Objects.requireNonNull(fileDir.list())) {
                    String name = fileName.replace(".txt", "").replace("_", " ");
                    AboutText aboutText = new AboutText(name, loadTextContent(fileName));
                    aboutTexts.add(aboutText);
                }
            }
        } else {
            LOGGER.error("about folder not found...");
        }
        if (aboutTexts.isEmpty()) {
            aboutTexts.add(new AboutText("no content", ""));
        }
        return aboutTexts;
    }

    private String loadTextContent(String fileName) {
        return Optional.ofNullable(getClass().getResource(RELATIVE_TEXT_RESOURCE_DIR + "/" + fileName))
                .stream()
                .flatMap(AboutService::toLines)
                .collect(Collectors.joining(format("%n")));
    }

    private static Stream<? extends String> toLines(URL url) {
        try {
            return Files.lines(new File(url.getFile()).toPath());
        } catch (IOException e) {
            LOGGER.error("File with path " + url + " not found...", e);
            return Stream.empty();
        }
    }

    public static class AboutText {

        private final String title;
        private final StringProperty text = new SimpleStringProperty();

        public AboutText(String title, String text) {
            this.title = title;
            this.text.set(text);
        }

        public String getText() {
            return text.get();
        }

        @Override
        public String toString() {
            return title;
        }
    }
}

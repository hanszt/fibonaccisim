package hzt.service;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

public class MouseControlService implements IMouseControlService {

    private double mouseAnchorX;
    private double mouseAnchorY;

    private double nodeAnchorTranslateX = 0;
    private double nodeAnchorTranslateY = 0;

    private final Node target;

    public MouseControlService(Node target) {
        this.target = target;
    }

    public void initMouseControl(Node reference) {
        reference.setOnMousePressed(this::mousePressedEvent);
        reference.setOnMouseDragged(this::mouseDraggedEvent);
    }

    private void mousePressedEvent(MouseEvent event) {
        mouseAnchorX = event.getX();
        mouseAnchorY = event.getY();
        nodeAnchorTranslateX = target.getTranslateX();
        nodeAnchorTranslateY = target.getTranslateY();
    }

    private void mouseDraggedEvent(MouseEvent event) {
        double deltaX = mouseAnchorX - event.getX();
        double deltaY = mouseAnchorY - event.getY();
        target.setTranslateX(nodeAnchorTranslateX - deltaX);
        target.setTranslateY(nodeAnchorTranslateY - deltaY);
    }

    public void setTargetTranslation(Point2D point) {
        target.setTranslateX(point.getX());
        target.setTranslateY(point.getY());
    }

}

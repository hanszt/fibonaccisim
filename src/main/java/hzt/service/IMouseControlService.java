package hzt.service;

import javafx.geometry.Point2D;
import javafx.scene.Node;

public interface IMouseControlService {

    void initMouseControl(Node node);

    void setTargetTranslation(Point2D point);

}

package hzt.controller.sub_pane;

import hzt.controller.FXMLController;
import hzt.controller.SceneManager;
import hzt.controller.scene.MainSceneController;
import hzt.controller.scene.SceneController;
import hzt.model.AppConstants;
import hzt.model.Resource;
import hzt.model.fibonacci.FibonacciGroup;
import hzt.service.BackgroundService;
import hzt.service.IBackgroundService;
import hzt.service.IThemeService;
import hzt.service.ThemeService;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;

import static hzt.model.AppConstants.INIT_BG_COLOR;
import static hzt.model.AppConstants.INIT_HIGHLIGHTING_COLOR;
import static java.lang.Boolean.TRUE;
import static java.util.function.Predicate.*;

public class AppearanceController extends FXMLController {

    public static final int CHANGE_MILLIS = 1000;
    private static final double STAGE_OPACITY = AppConstants.parsedDoubleProp("stage_opacity", .8D);
    private static final String INIT_THEME_FILE_NAME = AppConstants.getProperty("init_theme", "");
    private static final String INIT_BG_FILE_NAME = AppConstants.getProperty("init_background", "");

    @FXML
    private ToggleButton opacityStageButton;
    @FXML
    private ComboBox<Resource> backgroundCombobox;
    @FXML
    private ComboBox<Resource> themeCombobox;
    @FXML
    private ToggleButton fullScreenButton;
    @FXML
    private ColorPicker backgroundColorPicker;
    @FXML
    private ColorPicker highlightingColorPicker;
    @FXML
    private ColorPicker seedColorPicker;

    private final IThemeService themeService = new ThemeService();
    private final IBackgroundService backgroundService = new BackgroundService();
    private final MainSceneController mainSceneController;

    public AppearanceController(MainSceneController mainSceneController) throws IOException {
        super("appearancePane.fxml");
        this.mainSceneController = mainSceneController;

    }

    public void setup(FibonacciGroup fibonacciGroup) {
        reset();
        backgroundColorPicker.setValue(INIT_BG_COLOR);
        configureColorPickers(fibonacciGroup);
        configureComboBoxes();
        setBackgroundColor();
        configureStageControlButtons(mainSceneController.getSceneManager().getStage());
    }

    public void reset() {
        seedColorPicker.setValue(AppConstants.INIT_SHAPE_COLOR);
        highlightingColorPicker.setValue(INIT_HIGHLIGHTING_COLOR);
    }

    public void configureColorPickers(FibonacciGroup fibonacciGroup) {
        backgroundColorPicker.valueProperty().addListener(this::changeBackgroundColor);
        configureGraduallyChangingColorPickerAction(fibonacciGroup.objectColorProperty(), seedColorPicker);
        configureGraduallyChangingColorPickerAction(fibonacciGroup.highlightingColorProperty(), highlightingColorPicker);
    }

    private static void configureGraduallyChangingColorPickerAction(ObjectProperty<Color> colorProperty, ColorPicker colorPicker) {
        colorProperty.set(colorPicker.getValue());
        colorPicker.valueProperty().addListener((o, c, n) -> graduallyChangeColor(colorProperty, c, n));
    }

    private static void graduallyChangeColor(ObjectProperty<Color> colorProperty, Color from, Color to) {
        Timeline timeline = new Timeline(
                new KeyFrame(Duration.ZERO, new KeyValue(colorProperty, from)),
                new KeyFrame(Duration.millis(CHANGE_MILLIS), new KeyValue(colorProperty, to)));
        timeline.playFromStart();
    }

    private void configureComboBoxes() {
        configureThemeCombobox();
        configureBackgroundCombobox();
    }

    private void configureBackgroundCombobox() {
        Set<Resource> bgResources = backgroundService.getResources();
        bgResources.forEach(r -> backgroundCombobox.getItems().add(r));
        backgroundCombobox.setValue(bgResources.stream()
                .filter(e -> e.getFileName().equals(INIT_BG_FILE_NAME))
                .findFirst().orElse(BackgroundService.NO_PICTURE));
        backgroundComboBoxAction();
    }

    private void configureThemeCombobox() {
        Set<Resource> themes = themeService.getThemes();
        themes.forEach(theme -> themeCombobox.getItems().add(theme));
        themeService.currentThemeProperty().bind(themeCombobox.valueProperty());
        themeService.styleSheetProperty().addListener(this::changeStyle);
        themeCombobox.setValue(themes.stream()
                .filter(e -> e.getFileName().equals(INIT_THEME_FILE_NAME))
                .findFirst().orElse(IThemeService.DEFAULT_THEME));
    }

    public void changeStyle(ObservableValue<? extends String> o, String c, String newVal) {
        SceneManager sceneManager = mainSceneController.getSceneManager();
        Collection<SceneController> sceneControllers = sceneManager.getSceneControllerMap().values();
        sceneControllers.stream()
                .map(SceneController::getScene)
                .map(Scene::getStylesheets)
                .forEach(styleSheets -> {
            styleSheets.removeIf(not(filter -> styleSheets.isEmpty()));
            Optional.ofNullable(newVal).ifPresent(styleSheets::add);
        });
    }

    public void configureStageControlButtons(Stage stage) {
        fullScreenButton.setOnAction(e -> stage.setFullScreen(!stage.isFullScreen()));
        stage.fullScreenProperty().addListener((o, c, n) -> fullScreenButton.setSelected(n));
        stage.addEventFilter(KeyEvent.KEY_PRESSED, key -> setFullScreenWhenF11Pressed(stage, key));
        bindStageOpacityToOpacityButton(stage);
    }

    private static void setFullScreenWhenF11Pressed(Stage stage, KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.F11) {
            stage.setFullScreen(!stage.isFullScreen());
        }
    }

    private void bindStageOpacityToOpacityButton(Stage stage) {
        opacityStageButton.selectedProperty().addListener((o, c, n) -> stage.setOpacity(TRUE.equals(n) ? STAGE_OPACITY : 1));
    }

    @FXML
    private void backgroundComboBoxAction() {
        Optional.ofNullable(backgroundCombobox.getValue().getPathToResource())
                .filter(not(String::isEmpty))
                .map(path -> backgroundService.getClass().getResourceAsStream(path))
                    .ifPresentOrElse(inputStream -> mainSceneController.getAnimationPane()
                            .setBackground(backGround(new Image(inputStream), mainSceneController.getAnimationPane())),
                            this::setBackgroundColor);
    }

    @NotNull
    private static Background backGround(Image image, AnchorPane animationPane) {
        return new Background(new BackgroundImage(image,
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER,
                new BackgroundSize(animationPane.getWidth(), animationPane.getHeight(),
                        false, false, false, true)));
    }

    private void changeBackgroundColor(ObservableValue<? extends Color> o, Color cur, Color newColor) {
        graduallyChangeBgColor(mainSceneController.getAnimationPane(), cur, newColor);
        backgroundCombobox.setValue(BackgroundService.NO_PICTURE);
    }

    public void graduallyChangeBgColor(Region region, Color fromColor, Color toColor) {
        ObjectProperty<Color> color = new SimpleObjectProperty<>(fromColor);
        Timeline timeline = new Timeline(
                new KeyFrame(Duration.ZERO, new KeyValue(color, fromColor)),
                new KeyFrame(Duration.millis(CHANGE_MILLIS), new KeyValue(color, toColor)));

        color.addListener((o, c, n) ->
                region.setBackground(new Background(new BackgroundFill(n, CornerRadii.EMPTY, Insets.EMPTY))));
        timeline.playFromStart();
    }

    private void setBackgroundColor() {
        mainSceneController.getAnimationPane().setBackground(new Background(
                new BackgroundFill(backgroundColorPicker.getValue(), CornerRadii.EMPTY, Insets.EMPTY)));
    }

    @Override
    protected FXMLController getBean() {
        return this;
    }


}

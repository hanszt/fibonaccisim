package hzt.controller.scene;

import hzt.controller.SceneManager;
import hzt.controller.sub_pane.AppearanceController;
import hzt.model.AnimationAttribute;
import hzt.model.CenteredSubScene;
import hzt.model.TurnFraction;
import hzt.model.fibonacci.FibonacciGroup;
import hzt.service.AnimationService;
import hzt.service.IMouseControlService;
import hzt.service.MouseControlService;
import hzt.service.StatisticsService;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.Camera;
import javafx.scene.Group;
import javafx.scene.ParallelCamera;
import javafx.scene.SubScene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;

import static hzt.model.AppConstants.*;
import static hzt.model.AppConstants.Scene.MAIN_SCENE;

public class MainSceneController extends SceneController {

    private static final Logger LOGGER = LogManager.getLogger(MainSceneController.class);
    private static final double TURN_FRACTION_SLIDER_RANGE = .02;
    private static final double ZOOM_FACTOR = 0.001;

    @FXML
    private Tab appearanceTab;
    @FXML
    private AnchorPane animationPane;
    @FXML
    private ComboBox<AnimationAttribute> attributeCombobox;
    @FXML
    private ComboBox<TurnFraction> turnFractionComboBox;

    @FXML
    private Slider distributionSlider;
    @FXML
    private Slider numberOfObjectsSlider;
    @FXML
    private Slider seedSizeSlider;
    @FXML
    private Slider turnFractionSlider;
    @FXML
    private Slider highlightingSlider;
    @FXML
    private Slider flowerSizeSlider;
    @FXML
    private Slider animationIncrementSlider;

    @FXML
    private Label frameRateStatsLabel;
    @FXML
    private Label runTimeLabel;
    @FXML
    private Label animationIncrementLabel;
    @FXML
    private Label groupSizeLabel;
    @FXML
    private Label highlightingLabel;
    @FXML
    private Label turnFractionLabel;
    @FXML
    private Label distributionLabel;

    private final Group subSceneRoot = new Group();
    private final CenteredSubScene subScene2D;
    private final AnimationService animationService;
    private final AppearanceController appearanceController = new AppearanceController(this);
    private final FibonacciGroup fibonacciGroup = new FibonacciGroup();
    private final IMouseControlService mouseControlService = new MouseControlService(fibonacciGroup.getMembers());
    private final StatisticsService statisticsService = new StatisticsService(getGeneralStatsLabelDto(), getFibonacciLabelDto());

    private final AnimationAttribute none = new AnimationAttribute("None", 0, 0, 0) {
        @Override
        public void updateSliderValue() {
            LOGGER.trace("No attribute selected...");
        }
    };

    private final AnimationAttribute turnFractionAttribute = new AnimationAttribute("Turn fraction", -1e-5, 1e-5, 1e-7) {
        @Override
        public void updateSliderValue() {
            turnFractionSlider.setValue(turnFractionSlider.getValue() + animationIncrementSlider.getValue());
        }
    };

    private final AnimationAttribute distributionAttribute = new AnimationAttribute("Distribution", -.01, .01, -0.005) {
        @Override
        public void updateSliderValue() {
            distributionSlider.setValue(distributionSlider.getValue() + animationIncrementSlider.getValue());
        }
    };

    private final AnimationAttribute highLightingAttribute = new AnimationAttribute("Highlighting", -.1, .1, .05) {
        @Override
        public void updateSliderValue() {
            highlightingSlider.setValue(highlightingSlider.getValue() + animationIncrementSlider.getValue());
        }
    };

    private final AnimationAttribute memberCountAttribute = new AnimationAttribute("Member count", -10, 10, 1) {
        @Override
        public void updateSliderValue() {
            numberOfObjectsSlider.setValue(numberOfObjectsSlider.getValue() + animationIncrementSlider.getValue());
        }
    };

    public MainSceneController(SceneManager sceneManager) throws IOException {
        super(MAIN_SCENE.getFxmlFileName(), sceneManager);
        this.subScene2D = new CenteredSubScene(subSceneRoot, INIT_ANIMATION_PANE_DIMENSION.getWidth(),
                INIT_ANIMATION_PANE_DIMENSION.getHeight());
        this.animationService = new AnimationService(startTimeSim, statisticsService);
        this.animationPane.getChildren().add(subScene2D);
        this.appearanceTab.setContent(appearanceController.getRoot());
    }

    private StatisticsService.GeneralStatsLabelDto getGeneralStatsLabelDto() {
        StatisticsService.GeneralStatsLabelDto generalStatsLabelDto = new StatisticsService.GeneralStatsLabelDto();
        generalStatsLabelDto.setFrameRateLabel(frameRateStatsLabel);
        generalStatsLabelDto.setRunTimeLabel(runTimeLabel);
        generalStatsLabelDto.setAnimationIncrementLabel(animationIncrementLabel);
        return generalStatsLabelDto;
    }

    private StatisticsService.FibonacciGroupLabelDto getFibonacciLabelDto() {
        StatisticsService.FibonacciGroupLabelDto fibonacciGroupLabelDto = new StatisticsService.FibonacciGroupLabelDto();
        fibonacciGroupLabelDto.setGroupSizeLabel(groupSizeLabel);
        fibonacciGroupLabelDto.setHighlightingLabel(highlightingLabel);
        fibonacciGroupLabelDto.setTurnFractionLabel(turnFractionLabel);
        fibonacciGroupLabelDto.setDistributionLabel(distributionLabel);
        return fibonacciGroupLabelDto;
    }

    @Override
    public void setup() {
        configureAnimationPane(animationPane);
        configureSubScene(subScene2D, animationPane);
        setInitControlValues();
        configureComboBoxes();
        configureSliders();
        configureFibonacci(fibonacciGroup);
        subSceneRoot.getChildren().add(fibonacciGroup.getMembers());
        appearanceController.setup(fibonacciGroup);
        animationService.addAnimationLoopToTimeline(initializeAnimationLoop(), initializeStatisticsLoop());
    }

    private static void configureSubScene(SubScene subScene, Pane animationPane) {
        subScene.widthProperty().bind(animationPane.widthProperty());
        subScene.heightProperty().bind(animationPane.heightProperty());
        subScene.setCamera(getConfiguredCamera());
    }

    private static Camera getConfiguredCamera() {
        Camera camera = new ParallelCamera();
        camera.setFarClip(1000);
        camera.setNearClip(.01);
        return camera;
    }

    private static void configureAnimationPane(AnchorPane animationPane) {
        animationPane.setPrefSize(INIT_ANIMATION_PANE_DIMENSION.getWidth(), INIT_ANIMATION_PANE_DIMENSION.getHeight());
        animationPane.setBackground(new Background(new BackgroundFill(INIT_BG_COLOR, CornerRadii.EMPTY, Insets.EMPTY)));
    }

    private void configureComboBoxes() {
        attributeCombobox.getItems()
                .addAll(turnFractionAttribute, highLightingAttribute, distributionAttribute, memberCountAttribute, none);
        attributeCombobox.setValue(turnFractionAttribute);

        TurnFraction.configureComboBox(turnFractionComboBox);
        turnFractionComboBox.valueProperty().addListener(this::turnFractionComboboxValueChanged);
    }

    private void turnFractionComboboxValueChanged(ObservableValue<? extends TurnFraction> o,
                                                  TurnFraction c, TurnFraction n) {
        boolean parsable = n != null && (n.isParsable() || turnFractionComboBox.getItems().contains(n));
        if (parsable) {
            setTurnFractionSliderBounds(n);
            fibonacciGroup.angleFractionProperty().setValue(n.getValue());
            if (!turnFractionComboBox.getItems().contains(n)) {
                turnFractionComboBox.getItems().add(n);
            }
        }
    }

    private void setTurnFractionSliderBounds(TurnFraction n) {
        turnFractionSlider.setMin(n.getValue() - TURN_FRACTION_SLIDER_RANGE / 2);
        turnFractionSlider.setMax(n.getValue() + TURN_FRACTION_SLIDER_RANGE / 2);
    }

    private void configureFibonacci(FibonacciGroup fibonacciGroup) {
        fibonacciGroup.shapeSizeProperty().bind(seedSizeSlider.valueProperty());
        mouseControlService.initMouseControl(animationPane);
        animationPane.setOnScroll(this::mouseScrollEvent);
        fibonacciGroup.animationAttributeProperty().set(attributeCombobox.getValue());
        fibonacciGroup.getAnimationAttribute().updateIncrementSliderBounds(animationIncrementSlider);
    }

    private void mouseScrollEvent(ScrollEvent event) {
        double delta = event.getDeltaY();
        changeOnScroll(delta, flowerSizeSlider);
        changeOnScroll(delta, seedSizeSlider);
    }

    private static void changeOnScroll(double delta, Slider slider) {
        double min = slider.getMin();
        double value = slider.getValue();
        double max = slider.getMax();

        delta = (delta * (max - min)) * ZOOM_FACTOR;
        slider.setValue(value - delta);
    }

    private EventHandler<ActionEvent> initializeAnimationLoop() {
        return loop -> animationService.run(fibonacciGroup);
    }

    private EventHandler<ActionEvent> initializeStatisticsLoop() {
        return loop -> animationService.runStatistics(fibonacciGroup, animationIncrementSlider.getValue() / INIT_FRAME_RATE);
    }

    private void configureSliders() {
        fibonacciGroup.angleFractionProperty().bindBidirectional(turnFractionSlider.valueProperty());
        fibonacciGroup.distributionFactorProperty().bind(distributionSlider.valueProperty());
        fibonacciGroup.spiralGroupDimensionProperty().bindBidirectional(flowerSizeSlider.valueProperty());
        fibonacciGroup.highlightingValueProperty().bind(highlightingSlider.valueProperty());
        fibonacciGroup.memberSizeProperty().bind(numberOfObjectsSlider.valueProperty());

        numberOfObjectsSlider.valueProperty().addListener((o, c, n) -> statisticsService.showGroupStatistics(fibonacciGroup));
        turnFractionSlider.valueProperty().addListener((o, c, n) -> updatePosition());
        distributionSlider.valueProperty().addListener((o, c, n) -> updatePosition());
        flowerSizeSlider.valueProperty().addListener((o, c, n) -> updatePosition());
        highlightingSlider.valueProperty().addListener((o, c, n) -> updateHighlighting());
    }

    private void updatePosition() {
        fibonacciGroup.updatePosition();
        statisticsService.showGroupStatistics(fibonacciGroup);
    }


    private void updateHighlighting() {
        fibonacciGroup.updateHighlighting();
        statisticsService.showGroupStatistics(fibonacciGroup);
    }


    private void setInitControlValues() {
        numberOfObjectsSlider.setValue(INIT_NUMBER_OF_SHAPES);
        seedSizeSlider.setValue(INIT_SEED_SIZE);
        distributionSlider.setValue(DISTRIBUTION_FACTOR);
        turnFractionSlider.setValue(GOLDEN_RATIO);
        flowerSizeSlider.setValue(INIT_FLOWER_RADIUS);
        highlightingSlider.setValue(INIT_FIBONACCI_NUMBER_INDEX);
    }

    @FXML
    private void pauseSimButtonAction(ActionEvent actionEvent) {
        if (((ToggleButton) actionEvent.getSource()).isSelected()) {
            animationService.pauseAnimationTimeline();
        } else {
            animationService.startAnimationTimeline();
        }
    }

    @FXML
    private void resetButtonAction() {
        setInitControlValues();
        turnFractionComboBox.setValue(TurnFraction.getGoldenRatio());
        fibonacciGroup.reset();
    }

    @FXML
    private void distributionButtonAction() {
        distributionSlider.setValue(DISTRIBUTION_FACTOR);
    }

    @FXML
    private void goldenRatioButtonAction() {
        fibonacciGroup.angleFractionProperty().set(GOLDEN_RATIO);
        turnFractionComboBox.setValue(TurnFraction.getGoldenRatio());
    }

    @FXML
    private void animatedAttributeComboboxAction() {
        fibonacciGroup.animationAttributeProperty().setValue(attributeCombobox.getValue());
        attributeCombobox.getValue().updateIncrementSliderBounds(animationIncrementSlider);
    }

    protected SceneController getBean() {
        return this;
    }

    @FXML
    private void recenterButtonAction() {
        mouseControlService.setTargetTranslation(new Point2D(0, 0));
        flowerSizeSlider.setValue(INIT_FLOWER_RADIUS);
        seedSizeSlider.setValue(INIT_SEED_SIZE);
    }

    public AnchorPane getAnimationPane() {
        return animationPane;
    }
}
